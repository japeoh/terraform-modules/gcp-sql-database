locals {
  db_name = format("%s-%s", replace(module.meta.name_suffix, ".", "-"), random_id.db_name_suffix.hex)
  labels  = module.meta.labels
}
