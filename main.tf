resource google_sql_database_instance db {
  name             = local.db_name
  database_version = var.db_version
  region           = var.gcp_region
  project          = var.gcp_project

  settings {
    tier = var.db_tier
  }
}

resource random_id db_name_suffix {
  byte_length = 5
}

resource random_password admin_password {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource google_sql_user admin {
  instance = google_sql_database_instance.db.name
  name     = var.db_admin_user
  password = random_password.admin_password.result
  project  = var.gcp_project
}

resource google_sql_database database {
  count = length(var.db_names)

  name     = element(var.db_names, count.index)
  instance = google_sql_database_instance.db.name
  project  = var.gcp_project
}
