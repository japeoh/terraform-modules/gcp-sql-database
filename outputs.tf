output ip_address {
  value = google_sql_database_instance.db.first_ip_address
}

output admin_user {
  value     = google_sql_user.admin.name
  sensitive = true
}

output admin_password {
  value     = google_sql_user.admin.password
  sensitive = true
}
