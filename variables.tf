variable domain {}
variable unit {}
variable environment {}
variable project {}
variable gcp_project {}
variable gcp_region {}
variable db_version {
  default = "POSTGRES_11"
}
variable db_tier {
  default = "db-f1-micro"
}
variable db_admin_user {
  default = "admin"
}
variable db_names {
  type    = list(string)
  default = []
}
